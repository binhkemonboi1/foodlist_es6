const BASE_URL = "https://64d71fc42a017531bc12fd33.mockapi.io/food";


let getList = () => {
    return axios({
        url: BASE_URL,
        method: "GET",
    });
};
let deleteFood = (id) => {
    return axios({
        url: `${BASE_URL}/${id}`,
        method: "DELETE",
    });
};
let addFood = (food) => {
    return axios ({
        url: BASE_URL,
        method: "POST",
        data: food,
    });
};
let getDetail = (id) => {
    return axios ({
        url: `${BASE_URL}/${id}`,
        method: "GET",
    });
};
let updateFood = () => { 
    return axios ({
        url: BASE_URL,
        method: "POST",
    });
};

let foodServ = {
    getList,
    deleteFood,
    addFood,
    getDetail,
    updateFood,
};
export default foodServ;