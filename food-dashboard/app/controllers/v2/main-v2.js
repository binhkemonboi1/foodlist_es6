import { onSuccess, renderFoodList, showDataForm } from "./controller-v2.js";
import foodServ from "../../service/service.js"
import { layThongTin } from "../v1/controller-v1.js"

const BASE_URL = "https://64d71fc42a017531bc12fd33.mockapi.io/food";

// render food list
let fetchFoodList = () => {
foodServ
.getList()

.then((res) => {
    console.log(res);
    renderFoodList(res.data);
})
.catch((err) => {
    console.log(err);
});
};
fetchFoodList();
// tách service

let deleteFood = (id) => { 
    axios({
        url: `${BASE_URL}/${id}`,
        method: "DELETE",
    })
    .then((res) => {
        console.log(res);
        onSuccess("Xóa thành công");
        fetchFoodList();
    })
    .catch((err) => {
        console.log(err);
    });
};
window.deleteFood = deleteFood;

// window.addFood = addFood;
window.addFood = () => {
    console.log("yes");
    let data = layThongTin();
    foodServ
    .addFood(data)
    .then((res) => {
        $("#exampleModal").modal("hide");
        onSuccess("Thêm thành công");
        fetchFoodList();
    })
    .catch((err) => {
        console.log(err);
    });

};

window.editFood = (id) => {
    $("#exampleModal").modal("show");
    foodServ
        .getDetail(id)
        .then((res) => {
                console.log(res);
                showDataForm(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
};

window.updateFood = () => {
    foodServ
    .updateFood()
    .then((res) => {
        console.log(res);
        $("#exampleModal").modal("hide");
        onSuccess("Cập nhật thành công");
        fetchFoodList();
    })
    .catch((err) => {
        console.log(err);
    });

};

